import java.util.Scanner;

public class Lingkaran {
    static void luasLingkaran(){
        Scanner scan = new Scanner(System.in);
        double luas, phi=3.14;
        int r;

        System.out.println("+---------------------------------+");
        System.out.println("|      Kamu Memilih Lingkaran     |");
        System.out.println("+---------------------------------+");
        System.out.print(" Masukkan r = ");
        r = scan.nextInt();
        luas = phi*r*r;
        System.out.println("---------------------------------");
        System.out.println(" Luas lingkaran adalah : " + luas + " cm");
    }
}
